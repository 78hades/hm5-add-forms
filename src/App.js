import "./App.scss";
import { useEffect } from "react";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home/home";
import BasketProducts from "./pages/Basket/Basket.jsx";
import Favorites from "./pages/Wishlist/Wishlist";
import { useSelector, useDispatch } from "react-redux";
import {
  lSCountBasket,
  lSbasket,
  lSGrandTotal,
} from "./redux/basket.slice/basket.slice";
import {
  lSCountWishlist,
  lSWishlist,
} from "./redux/wishlist.slice/wishlist.slice";
import Checkout from "./pages/Checkout/Checkout.jsx";
function App() {
  const { countBasket, basket, grandTotal } = useSelector(
    (state) => state.basket
  );
  const { countWishlist, wishlist } = useSelector((state) => state.wishlist);
  const dispatch = useDispatch();

  useEffect(() => {
    const newValueBasket = JSON.parse(localStorage.getItem("countBasket"));
    if (newValueBasket) {
      dispatch(lSCountBasket(newValueBasket));
    }
  }, [dispatch]);

  useEffect(() => {
    if (countBasket !== 0) {
      localStorage.setItem("countBasket", JSON.stringify(countBasket));
    } else {
      localStorage.removeItem("countBasket");
    }
  }, [countBasket]);

  useEffect(() => {
    const newValue = localStorage.getItem("basket");
    if (newValue) {
      dispatch(lSbasket(JSON.parse(newValue)));
    }
  }, [dispatch]);

  useEffect(() => {
    if (basket.length !== 0) {
      localStorage.setItem("basket", JSON.stringify(basket));
    } else {
      localStorage.removeItem("basket");
    }
  }, [basket]);

  useEffect(() => {
    const newValue = localStorage.getItem("countWishlist");
    if (newValue) {
      dispatch(lSCountWishlist(newValue));
    }
  }, [dispatch]);

  useEffect(() => {
    if (countWishlist !== 0) {
      localStorage.setItem("countWishlist", JSON.stringify(countWishlist));
    } else {
      localStorage.removeItem("countWishlist");
    }
  }, [countWishlist]);

  useEffect(() => {
    const newValue = JSON.parse(localStorage.getItem("wishlist"));
    if (newValue) {
      dispatch(lSWishlist(newValue));
    }
  }, [dispatch]);

  useEffect(() => {
    if (wishlist.length !== 0) {
      localStorage.setItem("wishlist", JSON.stringify(wishlist));
    } else {
      localStorage.removeItem("wishlist");
    }
  }, [wishlist]);

  useEffect(() => {
    dispatch(lSCountWishlist(wishlist.length));
  }, [dispatch, wishlist]);

  useEffect(() => {
    if (grandTotal !== 0) {
      localStorage.setItem("grandTotal", JSON.stringify(grandTotal));
    }
  }, [grandTotal]);

  useEffect(() => {
    const newValue = localStorage.getItem("grandTotal");
    if (newValue) {
      dispatch(lSGrandTotal(JSON.parse(newValue)));
    }
  });
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/basket" element={<BasketProducts />} />
        <Route path="/wishlist" element={<Favorites></Favorites>}></Route>
        <Route path="/checkout" element={<Checkout></Checkout>}></Route>
      </Routes>
      <Footer />
    </>
  );
}

export default App;
