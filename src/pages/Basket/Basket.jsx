import Product from "./Product";
import Modal from "../../components/Modal/Modal";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
function Basket() {
  const { basket, grandTotal } = useSelector((state) => state.basket);

  const CardProduct = basket.map((product, index) => (
    <Product key={index} product={product} />
  ));

  return (
    <>
      {" "}
      <p className="cart-page__path">
        <NavLink className="link" to="/">
          Home
        </NavLink>{" "}
        &gt;{" "}
        <NavLink className="link" activeclassname="active" to="/basket">
          Add to cart
        </NavLink>
      </p>
      {basket.length === 0 ? (
        <div className="cart-page__empty">
          <p className="cart-page__empty-message">
            You have not added any product to cart : &#40; . . . <br />
            Continue shopping ---&gt;{" "}
            <NavLink className="link-Home" to="/">
              Home
            </NavLink>{" "}
          </p>
        </div>
      ) : (
        <>
          <table className="cart-page">
            <tbody>
              <tr className="cart-page__products-details">
                <td>products details</td>
                <td>price</td>
                <td>shipping</td>
                <td>subtotal</td>
                <td>delete</td>
              </tr>
              {CardProduct}
            </tbody>
          </table>
          <aside className="cart-page__checkout">
            <p>
              Grand Total <span>${grandTotal}</span>
            </p>{" "}
            <NavLink to="/checkout">
              {" "}
              <button>Proceed to Check</button>
            </NavLink>
          </aside>
        </>
      )}
      <Modal></Modal>
    </>
  );
}

export default Basket;
