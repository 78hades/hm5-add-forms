import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  wishlist: [],
  countWishlist: 0,
  isWishlist: false,
};

const wishlistSlice = createSlice({
  name: "wishlist",
  initialState,
  reducers: {
    fillWishlist: (state, action) => {
      state.wishlist.push(action.payload);
    },
    clearWishlist: (state, action) => {
      const index = state.wishlist.findIndex(
        (product) => product.id === action.payload.id
      );
      if (index !== -1) {
        state.wishlist.splice(index, 1);
      }
    },
    isWishlistAction: (state, action) => {
      state.isWishlist = action.payload;
    },
    lSCountWishlist: (state, action) => {
      state.countWishlist = action.payload;
    },
    lSWishlist: (state, action) => {
      state.wishlist = action.payload;
    },
  },
});
export const {
  fillWishlist,
  clearWishlist,
  isWishlistAction,
  lSCountWishlist,
  lSWishlist,
} = wishlistSlice.actions;

export default wishlistSlice.reducer;
