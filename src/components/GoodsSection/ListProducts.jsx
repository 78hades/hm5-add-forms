import CardProduct from "./CartProduct";
import Modal from "../Modal/Modal";
import { fetchProducts } from "../../redux/products.slice/products.slice";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
function ListProducts() {
  const dispatch = useDispatch();
  const { products, status } = useSelector((state) => state.products);
  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  if (status === "error") {
    return <h1>No Products</h1>;
  }

  const cardProduct = products.map((product) => (
    <CardProduct key={product.id} product={product}></CardProduct>
  ));
  return (
    <>
      <h3 className="products__title">Product</h3>{" "}
      <section className="products">{cardProduct}</section> <Modal></Modal>
    </>
  );
}

export default ListProducts;
